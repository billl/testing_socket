#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LEN 1024

struct in_addr localInterface;
struct sockaddr_in groupSock;
struct stat filestat;
int sd;
char databuf[LEN];
int datalen = LEN;

int main(int argc, char *argv[])
{
	int fp, num_seg;
	if (argc < 1)
	{
		fprintf(stderr, "Specify a file to transfer\n");
		exit(1);
	}
	else
	{
		fp = open(argv[1], O_RDONLY);
		if (fp < 0)
		{
			fprintf(stderr, "Wrong file\n");
			exit(1);
		}
		fstat(fp, &filestat);
		num_seg = (filestat.st_size + LEN) / LEN;
	}
	/* Create a datagram socket on which to send. */
	sd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sd < 0)
	{
		perror("Opening datagram socket error");
		exit(1);
	}
	else
	{
		printf("Opening the datagram socket...OK.\n");
	}

	/* Initialize the group sockaddr structure with a */
	/* group address of 225.1.1.1 and port 5555. */
	memset((char *)&groupSock, 0, sizeof(groupSock));
	groupSock.sin_family = AF_INET;
	groupSock.sin_addr.s_addr = inet_addr("226.1.1.1");
	groupSock.sin_port = htons(4321);

	localInterface.s_addr = inet_addr("10.0.2.15");
	if (setsockopt(sd, IPPROTO_IP, IP_MULTICAST_IF, (char *)&localInterface, sizeof(localInterface)) < 0)
	{
		perror("Setting local interface error");
		exit(1);
	}
	else
	{
		printf("Setting the local interface...OK\n");
	}

	snprintf(databuf, datalen, "%s", basename(argv[1]));
	if (sendto(sd, databuf, datalen, 0, (struct sockaddr *)&groupSock, sizeof(groupSock)) < 0)
	{
		perror("Sending initial datagram message error");
	}
	else
	{
		printf("Sending initial datagram message...OK\n");
	}
	snprintf(databuf, datalen, "%d", num_seg);
	if (sendto(sd, databuf, datalen, 0, (struct sockaddr *)&groupSock, sizeof(groupSock)) < 0)
	{
		perror("Sending initial datagram message error");
	}
	else
	{
		printf("Sending initial datagram message...OK\n");
	}
	int bytes_read, index = 1;
	do
	{
		bytes_read = read(fp, databuf, datalen);
		if (sendto(sd, databuf, datalen, 0, (struct sockaddr *)&groupSock, sizeof(groupSock)) < 0)
		{
			perror("Sending datagram message error");
		}
		else
		{
			printf("Sending #%d datagram message...OK\n", index);
		}
		index++;
	} while (bytes_read == LEN);

	printf("transfer finished\n");
	return 0;
}