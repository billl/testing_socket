#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LEN 1024

struct sockaddr_in localSock;
struct ip_mreq group;
int sd;
int datalen;
char databuf[LEN];

int main(int argc, char *argv[])
{
	int fp;
	sd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sd < 0)
	{
		perror("Opening datagram socket error");
		exit(1);
	}
	else
	{
		printf("Opening datagram socket....OK.\n");
	}

	int reuse = 1;
	if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse, sizeof(reuse)) < 0)
	{
		perror("Setting SO_REUSEADDR error");
		close(sd);
		exit(1);
	}
	else
	{
		printf("Setting SO_REUSEADDR...OK.\n");
	}

	memset((char *)&localSock, 0, sizeof(localSock));
	localSock.sin_family = AF_INET;
	localSock.sin_port = htons(4321);
	localSock.sin_addr.s_addr = INADDR_ANY;
	if (bind(sd, (struct sockaddr *)&localSock, sizeof(localSock)))
	{
		perror("Binding datagram socket error");
		close(sd);
		exit(1);
	}
	else
	{
		printf("Binding datagram socket...OK.\n");
	}

	group.imr_multiaddr.s_addr = inet_addr("226.1.1.1");
	group.imr_interface.s_addr = inet_addr("10.0.2.15");
	if (setsockopt(sd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&group, sizeof(group)) < 0)
	{
		perror("Adding multicast group error");
		close(sd);
		exit(1);
	}
	else
	{
		printf("Adding multicast group...OK.\n");
	}

	int total_seg;
	if (read(sd, databuf, LEN) < 0)
	{
		perror("Reading initial datagram message error");
		close(sd);
		exit(1);
	}
	else
	{
		char name[1024];
		printf("Reading initial datagram message...OK.\n");
		printf("The message from multicast server is: \"%s\"\n", databuf);
		snprintf(name, sizeof(name), "received_%s", databuf);
		fp = open(name, O_WRONLY | O_APPEND | O_CREAT | O_TRUNC, S_IRWXU);
		if (fp < 0)
		{
			perror("Error Creating file");
			exit(1);
		}
	}
	if (read(sd, databuf, LEN) < 0)
	{
		perror("Reading initial datagram message error");
		close(sd);
		exit(1);
	}
	else
	{
		printf("Reading initial datagram message...OK.\n");
		printf("The message from multicast server is: \"%s\"\n", databuf);
	}
	sscanf(databuf, "%d", &total_seg);
	while (total_seg--)
	{
		if (read(sd, databuf, LEN) < 0)
		{
			perror("Reading datagram message error");
			close(sd);
			exit(1);
		}
		else
		{
			write(fp, databuf, LEN);
		}
	}

	printf("finished\n");

	return 0;
}
